import requests
from time import localtime, strftime
import copy
from operator import itemgetter
import os

# import sys
# sys.path.insert(0, 'C:\\1. Patryk\\Data science\\Python\\Kody')
# from Gmail import send_mail

os.chdir('C:\\1. Patryk\\Data science\\Python\\Kody\\Raporty')

#########  PARAMETERS #########

# Default values
IATA_departure = 'SXF'  # Flight departure location, IATA code
IATA_arrival = ''       # Flight arrival location, IATA code (optional)
Language = 'pl'         # Language in which airport names are displayed
Limit = '50'           # Maximum number of displayed flights 
Offset = '0'            # ? No idea, no official API documentation
Flex_in = '0'           # The number of extra days to include with DateIn 
Flex_out = '0'          # The number of extra days to include with DateOut 
Price_value = '200'     # Maximum prize for a one-way or two-way ticket
dep_out_date_from = '2018-04-28' # Earliest departure date of an outbound flight
dep_out_date_to = '2018-05-03'   # Latest departure date of an outbound flight
dep_in_date_from ='2018-05-11'   # Earliest departure date of an inbound flight
dep_in_date_to = '2018-05-30'    # Latest departure date of an inbound flight

def change_parameters():
    ''' User defines other parameters than default ones.
    Variables must be defined as global, until the code will not be
    rewritten with classes.
    '''

    global dep_out_date_from
    global dep_out_date_to 
    global dep_in_date_from 
    global dep_in_date_to 
    global Price_value 
    
    dep_out_date_from = input("Earliest departure date of an outbound flight (YYYY-MM-DD): ") 
    dep_out_date_to = input("Latest departure date of an outbound flight (YYYY-MM-DD): ") 
    dep_in_date_from = input("Earliest departure date of an inbound flight (YYYY-MM-DD): ") 
    dep_in_date_to = input("Latest departure date of an inbound flight (YYYY-MM-DD): ") 
    IATA_departure = input("Flight departure location, three-letter IATA airport code: ")
    Price_value = input("Maximum price for a one-way or two-way ticket (in departure location currency): ")
    
def show_parameters():
    ''' Displaying current parameters values'''

    print("Earliest departure date of an outbound flight: " + str(dep_out_date_from) + '\n') 
    print("Latest departure date of an outbound flight: " + str(dep_out_date_to) + '\n')
    print("Earliest departure date of an inbound flight: " + str(dep_in_date_from) + '\n')
    print("Latest departure date of an inbound flight: " + str(dep_in_date_to) + '\n')
    print("Maximum price for a one-way or two-way ticket (in departure location currency): " + str(Price_value) + '\n')


def get_url(ways):
    '''Creating an URL to query Ryanair website'''

    if ways ==1:
        if IATA_arrival:
            return 'https://api.ryanair.com/farefinder/3/oneWayFares?'  + \
            '&arrivalAirportIataCode=' + IATA_arrival + \
            '&departureAirportIataCode=' + IATA_departure + \
            '&language=' + Language +   '&limit=' + Limit + '&FlexDaysIn=' + \
            Flex_in + '&FlexDaysOut=' + Flex_out +'&market=' + Language + '-' + \
            Language + '&offset=' + Offset + '&outboundDepartureDateFrom=' + \
            dep_out_date_from + '&outboundDepartureDateTo=' + \
            dep_out_date_to + '&priceValueTo=' + Price_value 
        else:
            return 'https://api.ryanair.com/farefinder/3/oneWayFares?'  + \
            '&departureAirportIataCode=' + IATA_departure + \
            '&language=' + Language +   '&limit=' + Limit + '&FlexDaysIn=' + \
            Flex_in + '&FlexDaysOut=' + Flex_out +'&market=' + Language + '-' + \
            Language + '&offset=' + Offset + '&outboundDepartureDateFrom=' + \
            dep_out_date_from + '&outboundDepartureDateTo=' + \
            dep_out_date_to + '&priceValueTo=' + Price_value 

    if ways == 2:
        if IATA_arrival:	
            return 'https://api.ryanair.com/farefinder/3/roundTripFares?' + \
            '&arrivalAirportIataCode=' + IATA_arrival + \
            '&departureAirportIataCode=' + IATA_departure + \
            '&inboundDepartureDateFrom=' + dep_in_date_from + \
            '&inboundDepartureDateTo=' + dep_in_date_to + \
            '&language=' + Language +   '&limit=' + Limit + '&FlexDaysIn=' + \
            Flex_in + '&FlexDaysOut=' + Flex_out +'&market=' + Language + '-' + \
            Language + '&offset=' + Offset + '&outboundDepartureDateFrom=' + \
            dep_out_date_from + '&outboundDepartureDateTo=' + \
            dep_out_date_to + '&priceValueTo=' + Price_value 
        else:
            return 'https://api.ryanair.com/farefinder/3/roundTripFares?' + \
            '&departureAirportIataCode=' + IATA_departure + \
            '&inboundDepartureDateFrom=' + dep_in_date_from + \
            '&inboundDepartureDateTo=' + dep_in_date_to + \
            '&language=' + Language +   '&limit=' + Limit + '&FlexDaysIn=' + \
            Flex_in + '&FlexDaysOut=' + Flex_out +'&market=' + Language + '-' + \
            Language + '&offset=' + Offset + '&outboundDepartureDateFrom=' + \
            dep_out_date_from + '&outboundDepartureDateTo=' + \
            dep_out_date_to + '&priceValueTo=' + Price_value 

def find_fares(ways):
    '''Getting JSON data containing lowest fares from Ryanair website'''

    # Connecting to Ryanair website
    url = get_url(ways)
    r = requests.get(url) 
    if r.ok:
        print("Status code:", r.status_code)
    else:
        print("HTTP %i - %s, Message %s" % (r.status_code, r.reason, r.text))

    # Saving response  
    result  = r.json()
    return result['fares']


def get_vals(flight, key, one, two = 0):   
    '''Fares is a list of dictionaries that consist of 3 keys: outbound, inbound, summary
    We need to dig deeper inside their values, sub-values and sub-subvalues'''

    temp = []
    if two:
        temp = [v[one][two] for k,v in flight.items() if k == key]
    else:
        temp = [v[one] for k,v in flight.items() if k == key]
    return temp[0]


def clean_results(ways):
    ''' Results from JSON file are in a really ugly form. Let's make it more readable'''

    fares = find_fares(ways)
    flights_clean = []
    lowest_fares = {}
    
    for flight in fares:
        # Przygotowujemy slownik dla każdego kolejnego lotniska z listy
        lowest_fares['airport'] = get_vals(flight, 'outbound', 'arrivalAirport', 'name')
        lowest_fares['currency'] = get_vals(flight, 'outbound','price', 'currencyCode')
        lowest_fares['date'] =  get_vals(flight, 'outbound','departureDate')[:10]
        if ways == 2: lowest_fares['date_back'] = get_vals(flight,'inbound','departureDate')[:10]
        lowest_fares['price_main'] = int(get_vals(flight, 'summary', 'price', 'valueMainUnit'))
        lowest_fares['price_frac'] = int(get_vals(flight, 'summary', 'price', 'valueFractionalUnit'))
        lowest_fares['price_total'] = lowest_fares['price_main'] + lowest_fares['price_frac']/100
        flights_clean.append(copy.copy(lowest_fares)) # nasza zbiorcza lista wzbogaca sie o kolejny slownik-lotnisko, ALE UWAGA! Musi byc kopia, bo inaczej dane sie nadpisuja
    
    return flights_clean

def print_results(ways, flights_clean):
    ''' Printing out to the screen the lowest fares sorted by price '''

    print("\n\n" + IATA_departure, end='')
    
    if ways == 1:
        print(" - One-way trip: " + str(len(flights_clean)) + " offer(s) \n\n")
    if ways == 2:
        print(" - Two-way trip: " + str(len(flights_clean)) + " offer(s) \n\n")
    
    # itemgetter: sort the list of dictionaries by the price
    flights_clean = sorted(flights_clean, key=itemgetter('price_main'), reverse=False) 

    for flight in flights_clean:
        print (flight['airport'], end=": " )
        print (str(flight['price_total']) + flight['currency'], end='')
        if ways == 1:
            print(" on " + flight['date'], end="\n\n")
        if ways == 2:
            print(" on " + str(flight['date']) + ' - ' + str(flight['date_back']), end="\n\n")

def txt_results(ways, flights_clean):
    ''' Saving the lowest fares to Ryanair.txt file in the current directory '''

    with open("Ryanair.txt", 'a') as f:
        f.write("================================================ \n\n")
        f.write("Script was initialized: ")
        f.write(strftime('%Y-%m-%d %H:%M:%S', localtime()))
        f.write('\n\n')
        f.write("----------------------------------------------- \n\n")
        f.write(IATA_departure)
        
        if ways == 1:
            f.write('- One-way trip: \n\n')
        if ways == 2:
            f.write('- Two-way trip: \n\n')

        for flight in flights_clean:
            f.write(str(flight['airport']) + ": ")
            f.write(str(flight['price_total']) + ' ')
            f.write(str(flight['currency']) + ' on ')
            if ways == 1:
                f.write(str(flight['date']) + "\n\n")
            if ways == 2:
                f.write(str(flight['date']) + " - ")
                f.write(str(flight['date_back']) + "\n\n")

def one_way():
    ''' Getting clean results for one-way flights'''

    flights_clean = clean_results(1)
    print_results(1, flights_clean)
    txt_results(1, flights_clean)
    # send_mail(toaddr="patryk.wawrzyniak92@gmail.com", subject="Wyszukiwarka lotow Ryanair", body="Patrz: załącznik", att_path="C:\\1. Patryk\\Data science\\Python\\Kody\\Raporty", att_filename="Ryanair.txt")

def two_way():
    ''' Getting clean results for two-way flights'''
    
    flights_clean = clean_results(2)
    print_results(2, flights_clean)
    txt_results(2, flights_clean)
    # send_mail(toaddr="patryk.wawrzyniak92@gmail.com", subject="Wyszukiwarka lotow Ryanair", body="Patrz: załącznik", att_path="C:\\1. Patryk\\Data science\\Python\\Kody\\Raporty", att_filename="Ryanair.txt")
        
def menu():
    ''' Printing out the menu on the screen'''

    choice = None
    while choice != "0":
        print \
        ("""
        --------------- MENU ---------------
                
        0 - Exit
        1 - Insert flight data
        2 - Show current flight data
        3 - Search one-way tickets!
        4 - Search two-way tickets!
        
        """)
        
        choice = input("Choose: ") 
        print()
            
        if choice == "0":
            print("Bye bye!")  
        elif choice == "1":
            change_parameters()
        elif choice == "2":
            show_parameters()
        elif choice == "3":
            one_way()
        elif choice == "4":
            two_way()
        else:
            print(" !!!!!!!!! Wrong option !!!!!!!!! ")


menu()