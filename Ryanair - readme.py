"""
# Pomysly na dalsze rozwiniecie programu

# 1. Wyszukuje nie tylko lotow stricte 'return', ale tez lot do punktu A, 
# a powrot z punktu B oddalonego o X km w linii prostej

# 2. Tworzy macierz cen i wyszukuje na jej podstawie opcji cenowych zadanych dla okreslonej liczby dni

# 3. Moze tez z dodatkowym warunkiem wyjazdu i powrotu w konkretne dni tygodnia

# 4. Sprawdź cenę biletu, jeśli będzie mniejsza niż x zł, to informuj

# 5. Stworzyc klasy zamiast funkcji!

"""

# Co wiemy o ich API?

#	Flight info:
	#	https://api.ryanair.com/flightinfo/3/flights/?&arrivalAirportIataCode=SZZ&departureAirportIataCode=WAW&departureTimeScheduled \
	#	From=00:00&departureTimeScheduledTo=23:59&length=&number=&offset=

#	Currencies:
	#	https://desktopapps.ryanair.com/pl-pl/res/currencies

#	Calendar:
	#	https://desktopapps.ryanair.com/Calendar?Destination=WAW&IsTwoWay=TRUE&Months=16&Origin=SZZ&StartDate=2017-04-06

#	Schedule:
	#	https://api.ryanair.com/timetable/3/schedules/SZZ/WAW/years/2017/months/05

#	One-way Fares:
	# https://api.ryanair.com/farefinder/3/oneWayFares?&departureAirportIataCode=BCN&language=en&limit=16&market=en-gb&offset=0&outboundDepartureDateFrom=2016-10-11&outboundDepartureDateTo=2017-10-28&priceValueTo=150

# 	Cheapest per day:
	#	https://api.ryanair.com/farefinder/3/oneWayFares/SXF/TSR/cheapestPerDay?market=de-de&outboundMonthOfDate=2017-04-01
	
#	Round Trip Fares:
	# https://api.ryanair.com/farefinder/3/roundTripFares?&arrivalAirportIataCode=STN&departureAirportIataCode=VLC&inboundDepartureDateFrom=2016-10-11&inboundDepartureDateTo=2017-10-28&language=es&limit=16&market=es-es&offset=0&outboundDepartureDateFrom=2016-10-11&outboundDepartureDateTo=2017-10-28&priceValueTo=150

#	Airports (names, IATA, coordinates, time-zones):
	#	https://desktopapps.ryanair.com/en-gb/res/stations

#	All together:
	# https://api.ryanair.com/aggregate/3/common?embedded=airports,countries,cities,regions,nearbyAirports,defaultAirport&market=en-gb

#	Availability and fares info:
	#	https://desktopapps.ryanair.com/en-gb/availability?ADT=1&CHD=0&DateIn=2017-04-02&DateOut=2017-04-10&Destination=WAW&FlexDaysIn=6&FlexDaysOut=6&INF=0&Origin=SZZ&RoundTrip=true&TEEN=0

# Discounts
	#	https://api.ryanair.com/discount/3/discounts

# Markets/language codes:
	#	https://www.ryanair.com/content/ryanair.markets.json